#!/usr/bin/env bash
set -x

set -o errexit  # abort script at first error
set -o pipefail # return the exit status of the last command in the pipe

BASE="http://www.oracle.com"
#URL="$BASE/partners/secure/sales-tools/paas-iaas-public-cloud-supplement-2869897.pdf"

CLOUD_API_URL_OLD="https://itra.oraclecloud.com/itas/.anon/myservices/api/v1"
CLOUD_API_URL_NEW="https://www.oracle.com/a/ocom/docs/cloudestimator/js/pricing/v1/data"


function getCloudPricesOld() {
  filePath="Cloud/Price List/products.json"
  curl -o "$filePath" "${CLOUD_API_URL_OLD}/products?limit=500"
  prettier --write "$filePath" --tab-width 3
  #git add "$filePath"
  #git commit -m "Updates as of $(date -u +'%Y_%m_%d')"
}

function getCloudPrices() {
  filePath="Cloud/Price List/prices.json"
  curl -o "$filePath" "${CLOUD_API_URL_NEW}/prices.json"
  #curl -o "$filePath" "https://www.oracle.com/a/ocom/docs/pricing/cloud-price-list.json"
  prettier --write "$filePath" --tab-width 3
}

function getCloudProducts() {
  filePath="Cloud/Price List/product_list.json"
  curl -o "$filePath" "${CLOUD_API_URL_NEW}/products.json"
  prettier --write "$filePath" --tab-width 3
}

function getCloudProductPresets() {
  filePath="Cloud/Dependencies/presets.json"
  curl -o "$filePath" "${CLOUD_API_URL_OLD}/productPresets?limit=200"
  prettier --write "$filePath" --tab-width 3
}

function getCloudDiscounts() {
  filePath="Cloud/Price List/discounts.json"
  curl -o "$filePath" "${CLOUD_API_URL_OLD}/cloudCMDiscounts?limit=100"
  prettier --write "$filePath" --tab-width 3
  #offset=0
}

function getCloudPromotions() {
  filePath="Cloud/Price List/promotions.json"
  curl -o "$filePath" "https://myservices.us.oraclecloud.com/mycloud/rest/public/createCloudAccount/getSupportedPromotions"
  prettier --write "$filePath" --tab-width 3
  #offset=0
}

function getCloudManagedCloudPriceList() {
  filePath="Cloud/Managed Cloud Price List/$(date -u +'%Y_%m_%d').pdf"
  curl -o "$filePath" "${BASE}/assets/eploext-070596.pdf"
}

function getOnPremisesPriceList() {
  filePath="On-Premises/Price List/$(date -u +'%Y_%m_%d').pdf"
  curl -o "$filePath" "${BASE}/assets/technology-price-list-070617.pdf"
}

function getOnPremisesSupplement() {
  filePath="On-Premises/Price List/$(date -u +'%Y_%m_%d').pdf"
  curl -o "$filePath" "${BASE}/assets/paas-iaas-public-cloud-2140609.pdf"
}

function main() {
  getCloudPricesOld
  getCloudProductPresets
  getCloudDiscounts
  getCloudPrices
  getCloudProducts
  #getCloudPromotions
  #getCloudManagedCloudPriceList
  #getOnPremisesPriceList
}

main
