# Oracle Pricing

The following links are the original sources of the files in this repository.

## Price Lists

### On-Premises (PDF)
  
* [Commercial](https://www.oracle.com/assets/technology-price-list-070617.pdf)

* [Public Sector](https://www.oracle.com/assets/us-public-sector-3904395.pdf)

### Cloud (PDF and JSON)

#### Commercial

* [PDF](http://www.oracle.com/partners/secure/sales-tools/paas-iaas-public-cloud-price-list-2557381.pdf)

* [JSON](https://itra.oraclecloud.com/itas/.anon/myservices/api/v1/products?limit=500)

* [Managed Cloud (PDF)](https://www.oracle.com/assets/eploext-070596.pdf)

* [Discounts (JSON)](https://itra.oraclecloud.com/itas/.anon/myservices/api/v1/cloudCMDiscounts?limit=100)

#### Public Sector

* [PDF](https://www.oracle.com/assets/us-gov-tech-cloud-3902270.pdf)

## Cloud Documents

* [Licensing Oracle Software in the Cloud Computing Environment](https://www.oracle.com/assets/cloud-licensing-070579.pdf)

* [Supplement](http://www.oracle.com/partners/secure/sales-tools/paas-iaas-public-cloud-supplement-2869897.pdf)

* Service Descriptions
  * [Metered & Non-Metered](https://www.oracle.com/assets/paas-iaas-public-cloud-2140609.pdf)
  * [Universal Credits](https://www.oracle.com/assets/paas-iaas-universal-credits-3940775.pdf)
  * [Oracle University Cloud Services](https://www.oracle.com/assets/university-cloud-services-3095306.pdf)

* [Dependencies (JSON)](https://itra.oraclecloud.com/itas/.anon/myservices/api/v1/productPresets?limit=200)
* [Dependency Categories (JSON)](https://itra.oraclecloud.com/itas/.anon/myservices/api/v1/productPresetCategories?limit=100)
* [Partner Discount](http://www.oracle.com/partners/secure/sales-tools/partner-cs-discount-sched-1998746.pdf)

* [Pillar](https://www.oracle.com/assets/paas-iaas-pub-cld-srvs-pillar-4021422.pdf)
